import numpy as np
import pandas as pd
from keras import models, layers, optimizers
from keras import applications
from keras.preprocessing.image import ImageDataGenerator
import os
import tensorflow as tf
tf.get_logger().setLevel('INFO')
print('Done')

from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())
tf.test.gpu_device_name()