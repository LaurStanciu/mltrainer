﻿using MLTrainer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Models
{
    public class AuthenticateResponse
    {
        public UserResponse User { get; set; }
        public string Token { get; set; }

        public AuthenticateResponse(UserResponse user, string token)
        {
            User = user;
            Token = token;                
        }
    }
}
