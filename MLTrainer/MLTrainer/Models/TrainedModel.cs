﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Models
{
    [BsonIgnoreExtraElements]
    public class TrainedModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Algorithm Algorithm { get; set; }
        public string Status { get; set; }
        public string Path { get; set; }
        public List<string> Hyperparams { get; set; }

    }
}
