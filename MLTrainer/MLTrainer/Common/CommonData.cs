﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Common
{
    public class CommonData
    {
        public enum Roles
        {
            Admin,
            User
        }
        public enum Status
        {
            Ready,
            Training,
            Finished
        }

        public static ConcurrentDictionary<string,TrackedProcess> trackedProcessesDict = new ConcurrentDictionary<string,TrackedProcess>();
    }
}
