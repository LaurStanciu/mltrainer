﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Common
{
    public class TrackedProcess
    {
        public int PID { get; set; }
        public string ProcessName { get; set; }
    }
}
