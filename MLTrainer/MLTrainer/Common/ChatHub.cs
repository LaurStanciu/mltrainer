﻿using Microsoft.AspNetCore.SignalR;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Common
{
    public class ChatHub : Hub
    {
        public async Task Send( string trainedModelId)
        {
           await Clients.All.SendAsync("updatestatus", trainedModelId);
        }
    }
}
