﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Common
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string DataLocation { get; set; }
        public string DockerImageTitle { get; set; }
        public string AppName { get; set; }
        public int ProcessTrackerDelay { get; set; }
        public int IsLinux { get; set; }
        public string StartingArgs { get; set; }
        public string EndingArgs { get; set; }
        public int DifferentArgs { get; set; }
    }
}
