﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.DTO
{
    public class DatasetRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Filename { get; set; }
        public IFormFile FormFile { get; set; }
        public double Size { get; set; }
    }
}
