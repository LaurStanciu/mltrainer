﻿using MLTrainer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.DTO
{
    public class TrainedModelResponse
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Algorithm Algorithm { get; set; }
        public string Status { get; set; }
        public List<string> Hyperparams { get; set; }
    }
}
