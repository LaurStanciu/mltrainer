﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.DTO
{
    public class AlgorithmRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileContent { get; set; }
    }
}
