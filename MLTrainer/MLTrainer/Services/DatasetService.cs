﻿using MLTrainer.Interfaces;
using MLTrainer.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Services
{
    public class DatasetService: IDatasetService
    {
        private readonly IMongoCollection<Dataset> _dataset;
        private readonly IMongoCollection<User> _user;

        public DatasetService(IMLTrainerDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _dataset = database.GetCollection<Dataset>(settings.DatasetCollectionName);
            _user = database.GetCollection<User>(settings.UsersCollectionName);
        }

        public List<Dataset> Get() => _dataset.Find(dataset => true).ToList();

        public Dataset Get(string id) => _dataset.Find<Dataset>(dataset => dataset.Id == id).FirstOrDefault();

        public List<Dataset> GetByUserId(string userId) => _dataset.Find<Dataset>(dataset => dataset.UserId == userId).ToList();
        public List<Dataset> GetByUserIdAndCommon(string userId)
        {
            var commonUser = _user.Find<User>(user => user.Username == "Common").FirstOrDefault();
            var datasets = _dataset.Find<Dataset>(dataset => dataset.UserId == userId || dataset.UserId == commonUser.Id).ToList();
            return datasets;
        }

        public Dataset GetByTitle(string title) => _dataset.Find<Dataset>(dataset => dataset.Title.ToLower() == title.ToLower()).FirstOrDefault();

        public Dataset Create(Dataset dataset)
        {
            _dataset.InsertOne(dataset);
            return dataset;
        }

        public void Update(string id, Dataset dataset) =>
            _dataset.ReplaceOne(dataset => dataset.Id == id, dataset);

        public void Remove(Dataset datasetIn) =>
            _dataset.DeleteOne(dataset => dataset.Id == datasetIn.Id);

        public void Remove(string id) =>
            _dataset.DeleteOne(dataset => dataset.Id == id);
    }
}
