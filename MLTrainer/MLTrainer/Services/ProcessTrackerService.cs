﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MLTrainer.Common;
using MLTrainer.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MLTrainer.BackgroundServices
{
    public class ProcessTrackerService : BackgroundService
    {
        private readonly ILogger<ProcessTrackerService> logger;
        private readonly ITrainedModelService trainedModelsService;
        private readonly IHubContext<ChatHub> chatHubContext;
        private readonly AppSettings appSettings;

        public ProcessTrackerService(ILogger<ProcessTrackerService> logger, ITrainedModelService trainedModelsService, IHubContext<ChatHub> chatHubContext, IOptions<AppSettings> appSettings)
        {
            this.logger = logger;
            this.trainedModelsService = trainedModelsService;
            this.chatHubContext = chatHubContext;
            this.appSettings = appSettings.Value;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            logger.LogInformation("ProcessTrackerService running.");
            while (!stoppingToken.IsCancellationRequested)
            {
                foreach (KeyValuePair<string, TrackedProcess> entry in CommonData.trackedProcessesDict)
                {
                    TrackedProcess trackedProcess;
                    try
                    {
                        Process process = Process.GetProcessById(entry.Value.PID);
                        if (process.HasExited)
                        {
                            trainedModelsService.UpdateTrainedModelStatus(entry.Key, CommonData.Status.Finished);
                            chatHubContext.Clients.All.SendAsync("UpdateStatus", entry.Key);
                            logger.LogInformation("Removed " + entry.Key);
                            CommonData.trackedProcessesDict.TryRemove(entry.Key, out trackedProcess);
                        }
                    }
                    catch (Exception ex)
                    {
                        trainedModelsService.UpdateTrainedModelStatus(entry.Key, CommonData.Status.Finished);
                        chatHubContext.Clients.All.SendAsync("UpdateStatus", entry.Key);
                        logger.LogInformation("Removed " + entry.Key);
                        CommonData.trackedProcessesDict.TryRemove(entry.Key, out trackedProcess);
                    }
                    logger.LogInformation("ProcessTrackerService still running.");

                }

                Task.Delay(appSettings.ProcessTrackerDelay).Wait();
            }
            return Task.CompletedTask;
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            logger.LogInformation(
                "Consume Scoped Service Hosted Service is stopping.");

            await Task.CompletedTask;
        }
    }
}
