﻿using MLTrainer.Interfaces;
using MLTrainer.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Services
{
    public class AlgorithmService: IAlgorithmService
    {
        private readonly IMongoCollection<Algorithm> _algorithm;
        private readonly IMongoCollection<User> _user;

        public AlgorithmService(IMLTrainerDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _algorithm = database.GetCollection<Algorithm>(settings.AlgorithmCollectionName);
            _user = database.GetCollection<User>(settings.UsersCollectionName);
        }

        public List<Algorithm> Get() => _algorithm.Find(algorithm => true).ToList();

        public Algorithm Get(string id) => _algorithm.Find<Algorithm>(algorithm => algorithm.Id == id).FirstOrDefault();

        public List<Algorithm> GetByUserId(string userId) => _algorithm.Find<Algorithm>(algorithm => algorithm.UserId == userId).ToList();

        public List<Algorithm> GetByUserIdAndCommon(string userId) 
        {
            var commonUser = _user.Find<User>(user => user.Username == "Common").FirstOrDefault();
            var algorithms = _algorithm.Find<Algorithm>(algorithm => algorithm.UserId == userId || algorithm.UserId == commonUser.Id).ToList(); 
            return algorithms; 
        }

        public Algorithm GetByTitle(string title) => _algorithm.Find<Algorithm>(algorithm => algorithm.Title.ToLower() == title.ToLower()).FirstOrDefault();

        public Algorithm Create(Algorithm algorithm)
        {
            _algorithm.InsertOne(algorithm);
            return algorithm;
        }

        public void Update(string id, Algorithm algorithm) =>
            _algorithm.ReplaceOne(algorithm => algorithm.Id == id, algorithm);

        public void Remove(Algorithm algorithmIn) =>
            _algorithm.DeleteOne(algorithm => algorithm.Id == algorithmIn.Id);

        public void Remove(string id) =>
            _algorithm.DeleteOne(algorithm => algorithm.Id == id);
    }
}
