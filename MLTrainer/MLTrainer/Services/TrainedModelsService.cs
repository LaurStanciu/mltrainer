﻿using MLTrainer.Common;
using MLTrainer.Interfaces;
using MLTrainer.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace MLTrainer.Services
{
    public class TrainedModelsService : ITrainedModelService
    {
        private readonly IMongoCollection<TrainedModel> _trainedModels;
        private readonly IMongoCollection<User> _user;

        public TrainedModelsService(IMLTrainerDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _trainedModels = database.GetCollection<TrainedModel>(settings.TrainedModelsCollectionName);
            _user = database.GetCollection<User>(settings.UsersCollectionName);
        }

        public List<TrainedModel> Get() => _trainedModels.Find(trainedModel => true).ToList();

        public TrainedModel Get(string id) => _trainedModels.Find<TrainedModel>(trainedModel => trainedModel.Id == id).FirstOrDefault();

        public List<TrainedModel> GetByUserId(string userId) => _trainedModels.Find<TrainedModel>(trainedModel => trainedModel.UserId == userId).ToList();
        public List<TrainedModel> GetByUserIdAndCommon(string userId) 
        {
            var commonUser = _user.Find<User>(user => user.Username == "Common").FirstOrDefault();
            var trainedModels = _trainedModels.Find<TrainedModel>(trainedModel => trainedModel.UserId == userId || trainedModel.UserId == commonUser.Id).ToList();
            return trainedModels;
        }

        public TrainedModel GetByTitle(string title) => _trainedModels.Find<TrainedModel>(trainedModel => trainedModel.Title.ToLower() == title.ToLower()).FirstOrDefault();

        public TrainedModel Create(TrainedModel trainedModel)
        {
            _trainedModels.InsertOne(trainedModel);
            return trainedModel;
        }

        public void Update(string id, TrainedModel trainedModel) =>
            _trainedModels.ReplaceOne(trainedModel => trainedModel.Id == id, trainedModel);

        public string UpdateTrainedModelStatus(string trainedModelId, CommonData.Status status)
        {
            var update = Builders<TrainedModel>.Update.Set("Status", status.ToString());
            var updateResult = _trainedModels.UpdateOne<TrainedModel>(trainedModel => trainedModel.Id == trainedModelId, update);
            return updateResult.ToString();
        }

        public void Remove(TrainedModel trainedModelIn) =>
            _trainedModels.DeleteOne(trainedModel => trainedModel.Id == trainedModelIn.Id);

        public void Remove(string id) =>
            _trainedModels.DeleteOne(trainedModel => trainedModel.Id == id);
    }
}
