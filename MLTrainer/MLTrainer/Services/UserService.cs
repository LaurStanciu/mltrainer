﻿using AutoMapper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MLTrainer.Common;
using MLTrainer.DTO;
using MLTrainer.Interfaces;
using MLTrainer.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace MLTrainer.Services
{
    public class UserService: IUserService
    {
        private readonly IMongoCollection<User> _users;
        private readonly AppSettings _appSettings;
        private IMapper mapper;

        public UserService(IMLTrainerDatabaseSettings settings, IOptions<AppSettings> appSettings, IMapper mapper) 
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            this.mapper = mapper;

            _users = database.GetCollection<User>(settings.UsersCollectionName);
            _appSettings = appSettings.Value;
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = _users.Find<User>(user => user.Username.ToLower() == model.Username.ToLower() && user.Password == CommonFunctions.GetHash(model.Password)).FirstOrDefault();
            if (user == null) return null;
            var userResponse = mapper.Map<UserResponse>(user);
            var token = generateJwtToken(userResponse);

            return new AuthenticateResponse(userResponse, token);

        }

        public List<User> Get() => _users.Find(user => true).ToList();

        public User Get(string id) => _users.Find<User>(user => user.Id == id).FirstOrDefault();

        public User GetByUsername(string username) => _users.Find<User>(user => user.Username.ToLower() == username.ToLower()).FirstOrDefault();

        public User Create(User user)
        {
            user.Password = CommonFunctions.GetHash(user.Password);
            _users.InsertOne(user);
            return user;
        }

        public void Update(string id, User user) => 
            _users.ReplaceOne(user => user.Id == id, user);

        public void Remove(User userIn) =>
            _users.DeleteOne(user => user.Id == userIn.Id);

        public void Remove(string id) =>
            _users.DeleteOne(user => user.Id == id);

        private string generateJwtToken(UserResponse user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
