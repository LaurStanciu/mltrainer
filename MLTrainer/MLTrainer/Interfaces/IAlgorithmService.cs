﻿using MLTrainer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Interfaces
{
    public interface IAlgorithmService
    {
        public List<Algorithm> Get();
        public Algorithm Get(string id);
        public List<Algorithm> GetByUserId(string userId);
        public List<Algorithm> GetByUserIdAndCommon(string userId);
        public Algorithm GetByTitle(string name);
        public Algorithm Create(Algorithm algorithm);
        public void Update(string id, Algorithm algorithm);
        public void Remove(Algorithm algorithm);
        public void Remove(string id);
    }
}
