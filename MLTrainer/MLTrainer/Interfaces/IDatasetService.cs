﻿using MLTrainer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Interfaces
{
    public interface IDatasetService
    {
        public List<Dataset> Get();
        public Dataset Get(string id);
        public List<Dataset> GetByUserId(string userId);
        public List<Dataset> GetByUserIdAndCommon(string userId);
        public Dataset GetByTitle(string name);
        public Dataset Create(Dataset dataset);
        public void Update(string id, Dataset dataset);
        public void Remove(Dataset dataset);
        public void Remove(string id);
    }
}
