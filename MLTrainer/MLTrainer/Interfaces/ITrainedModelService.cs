﻿using MLTrainer.Common;
using MLTrainer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Interfaces
{
    public interface ITrainedModelService
    {
        public List<TrainedModel> Get();
        public TrainedModel Get(string id);
        public List<TrainedModel> GetByUserId(string userId);
        public List<TrainedModel> GetByUserIdAndCommon(string userId);
        public TrainedModel GetByTitle(string name);
        public TrainedModel Create(TrainedModel trainedModel);
        public void Update(string id, TrainedModel trainedModel);
        public string UpdateTrainedModelStatus(string trainedModelId, CommonData.Status status);
        public void Remove(TrainedModel trainedModelIn);
        public void Remove(string id);
    }
}
