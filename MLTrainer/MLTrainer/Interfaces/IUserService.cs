﻿using MLTrainer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Interfaces
{
    public interface IUserService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        public List<User> Get();
        public User Get(string id);
        public User GetByUsername(string username);
        public User Create(User user);
        public void Update(string id, User user);
        public void Remove(User userIn);
        public void Remove(string id);
    }
}
