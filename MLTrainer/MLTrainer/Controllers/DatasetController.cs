﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MLTrainer.Common;
using MLTrainer.DTO;
using MLTrainer.Interfaces;
using MLTrainer.Models;
using MLTrainer.Security;
using MLTrainer.Services;
using Newtonsoft.Json.Schema;
using static MLTrainer.Common.CommonData;

namespace MLTrainer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DatasetController : ControllerBase
    {
        private readonly IDatasetService datasetService;
        private AppSettings appSettings;
        private IMapper mapper;

        public DatasetController(IDatasetService datasetService,IOptions<AppSettings> appSettings, IMapper mapper)
        {
            this.datasetService = datasetService;
            this.appSettings = appSettings.Value;
            this.mapper = mapper;
        }

        [Authorize]
        [HttpGet("getbytitle/{title}")]
        public ActionResult<DatasetResponse> GetDataset(string title)
        {
            try
            {
                var dataset = datasetService.GetByTitle(title);
                var datasetResponse = mapper.Map<DatasetResponse>(dataset);
                return datasetResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpGet("getbyuserid/{id:length(24)}")]
        public ActionResult<List<DatasetResponse>> GetDatasets(string id)
        {
            try
            {
                var datasets = datasetService.GetByUserId(id);
                var datasetsResponse = mapper.Map<List<Dataset>, List<DatasetResponse>>(datasets);
                return datasetsResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpGet("getallavailable")]
        public ActionResult<List<DatasetResponse>> GetAllAvailableDatasets(string id)
        {
            try
            {
                var user = (UserResponse)HttpContext.Items["User"];
                var datasets = datasetService.GetByUserIdAndCommon(user?.Id);
                var datasetsResponse = mapper.Map<List<Dataset>, List<DatasetResponse>>(datasets);
                return datasetsResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }


        [Authorize]
        [HttpGet("{id:length(24)}", Name = "GetDataset")]
        public ActionResult<DatasetResponse> Get(string id)
        {
            try
            {
                var dataset = datasetService.Get(id);

                if (dataset == null)
                {
                    return NotFound();
                }
                var datasetResponse = mapper.Map<DatasetResponse>(dataset);
                return datasetResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpPost("create")]
        public ActionResult<DatasetResponse> Create([FromForm] DatasetRequest datasetRequest)
        {
            try
            {
                var dataset = mapper.Map<Dataset>(datasetRequest);
             
                    var user = (UserResponse)HttpContext.Items["User"];
                dataset.UserId = user?.Id;
                dataset.Path = user?.Id + @"\datasets\" + dataset.Title.Replace(" ", "_") + ".zip";

                string path = Path.Combine(appSettings.DataLocation, dataset.Path);

                if (appSettings.IsLinux == 1)
                {
                    dataset.Path = dataset.Path.Replace(@"\", "/");
                    path = path.Replace(@"\", "/");
                }

                Directory.CreateDirectory(Path.GetDirectoryName(path));
                using (Stream stream = new FileStream(path, FileMode.Create))
                {
                    datasetRequest.FormFile.CopyTo(stream);
                }


                datasetService.Create(dataset);
                return CreatedAtRoute("GetAlgorithm", new { id = dataset.Id.ToString() }, dataset);
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() });
            }
        }

        [Authorize]
        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, DatasetRequest datasetRequestIn)
        {
            try
            {
                var dataset = mapper.Map<Dataset>(datasetRequestIn);
                var datasetDB = datasetService.Get(id);

                if (datasetDB == null)
                {
                    return NotFound();
                }

                dataset.UserId = datasetDB.UserId;
                dataset.Path = datasetDB.UserId + @"\datasets\" + dataset.Title.Replace(" ", "_") + ".zip";
                string path = Path.Combine(appSettings.DataLocation, dataset.Path);

                if (appSettings.IsLinux == 1)
                {
                    dataset.Path = dataset.Path.Replace(@"\", "/");
                    path = path.Replace(@"\", "/");
                }

                Directory.CreateDirectory(Path.GetDirectoryName(path));
                using (Stream stream = new FileStream(path, FileMode.Create))
                {
                    datasetRequestIn.FormFile.CopyTo(stream);
                }
                datasetService.Update(id, dataset);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            try
            {
                var dataset = datasetService.Get(id);

                if (dataset == null)
                {
                    return NotFound();
                }

                System.IO.File.Delete(Path.Combine(appSettings.DataLocation, dataset.Path));

                datasetService.Remove(dataset.Id);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }
    }
}
