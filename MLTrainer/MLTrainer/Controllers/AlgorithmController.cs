﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MLTrainer.Common;
using MLTrainer.DTO;
using MLTrainer.Interfaces;
using MLTrainer.Models;
using MLTrainer.Security;
using MLTrainer.Services;
using Newtonsoft.Json.Schema;
using static MLTrainer.Common.CommonData;

namespace MLTrainer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlgorithmController : ControllerBase
    {
        private readonly IAlgorithmService algorithmService;
        private AppSettings appSettings;
        private IMapper mapper;

        public AlgorithmController(IAlgorithmService algorithmService, IOptions<AppSettings> appSettings, IMapper mapper)
        {
            this.algorithmService = algorithmService;
            this.appSettings = appSettings.Value;
            this.mapper = mapper;
        }

        [Authorize]
        [HttpGet("getbytitle/{title}")]
        public ActionResult<AlgorithmResponse> GetAlgorithm(string title)
        {
            try
            {
                var algorithm = algorithmService.GetByTitle(title);
                var algorithmResponse = mapper.Map<AlgorithmResponse>(algorithm);
                return algorithmResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpGet("getbyuserid/{id:length(24)}")]
        public ActionResult<List<AlgorithmResponse>> GetAlgorithms(string id)
        {
            try
            {
                var algorithms = algorithmService.GetByUserId(id);
                var algorithmsResponse = mapper.Map<List<Algorithm>, List<AlgorithmResponse>>(algorithms);
                return algorithmsResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpGet("getallavailable")]
        public ActionResult<List<AlgorithmResponse>> GetAllAvailableAlgorithms()
        {
            try
            {
                var user = (UserResponse)HttpContext.Items["User"];
                var algorithms = algorithmService.GetByUserIdAndCommon(user.Id);
                var algorithmsResponse = mapper.Map<List<Algorithm>, List<AlgorithmResponse>>(algorithms);
                return algorithmsResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }


        [Authorize]
        [HttpGet("{id:length(24)}", Name = "GetAlgorithm")]
        public ActionResult<AlgorithmResponse> Get(string id)
        {
            try
            {
                var algorithm = algorithmService.Get(id);

                if (algorithm == null)
                {
                    return NotFound();
                }
                var algorithmResponse = mapper.Map<AlgorithmResponse>(algorithm);
                return algorithmResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpPost("create")]
        public ActionResult<AlgorithmResponse> Create([FromBody] AlgorithmRequest algorithmRequest)
        {
            try
            {
                var algorithm = mapper.Map<Algorithm>(algorithmRequest);
                var user = (UserResponse)HttpContext.Items["User"];
                algorithm.UserId = user?.Id;

                algorithm.Path = user?.Id+ @"\algorithms\" + algorithm.Title.Replace(" ", "_") + ".py";
                string path = Path.Combine(appSettings.DataLocation, algorithm.Path);

                if (appSettings.IsLinux == 1)
                {
                    algorithm.Path = algorithm.Path.Replace(@"\", "/");
                    path = path.Replace(@"\", "/");
                }

                Directory.CreateDirectory(Path.GetDirectoryName(path));

                System.IO.File.WriteAllText(path, algorithm.FileContent);

                algorithmService.Create(algorithm);
                return CreatedAtRoute("GetAlgorithm", new { id = algorithm.Id.ToString() }, algorithm);
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() });
            }
        }

        [Authorize]
        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, AlgorithmRequest algorithmRequestIn)
        {
            try
            {
                var algorithm = mapper.Map<Algorithm>(algorithmRequestIn);
                var algorithmDB = algorithmService.Get(id);

                if (algorithmDB == null)
                {
                    return NotFound();
                }

                algorithm.UserId = algorithmDB.UserId;
                algorithm.Path = algorithmDB.UserId + @"\algorithms\" + algorithm.Title.Replace(" ", "_") + ".py";
                string path = Path.Combine(appSettings.DataLocation, algorithm.Path);

                if (appSettings.IsLinux == 1)
                {
                    algorithm.Path = algorithm.Path.Replace(@"\", "/");
                    path = path.Replace(@"\", "/");
                }

                Directory.CreateDirectory(Path.GetDirectoryName(path));

                System.IO.File.WriteAllText(path, algorithm.FileContent);

                algorithmService.Update(id, algorithm);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            try
            {
                var algorithm = algorithmService.Get(id);

                if (algorithm == null)
                {
                    return NotFound();
                }
                
                System.IO.File.Delete(Path.Combine(appSettings.DataLocation, algorithm.Path));
                algorithmService.Remove(algorithm.Id);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }
    }
}
