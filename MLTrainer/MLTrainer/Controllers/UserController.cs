﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MLTrainer.Common;
using MLTrainer.DTO;
using MLTrainer.Interfaces;
using MLTrainer.Models;
using MLTrainer.Security;
using MLTrainer.Services;
using Newtonsoft.Json;

namespace MLTrainer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        private IMapper mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        [Authorize]
        [HttpGet("getbyusername/{username}")]
        public ActionResult<UserResponse> GetUser(string username)
        {
            try
            {
                var user = userService.GetByUsername(username);
                var userResponse = mapper.Map<UserResponse>(user);
                return userResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() });
            }
        }

        [HttpGet("{id:length(24)}", Name = "GetUser")]
        public ActionResult<UserResponse> Get(string id)
        {
            try
            {
                var user = userService.Get(id);

                if (user == null)
                {
                    return NotFound();
                }

                var userResponse = mapper.Map<UserResponse>(user);

                return userResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() });
            }
        }


        [HttpPost("create")]
        public ActionResult<UserResponse> Create([FromBody] User user)
        {
            try
            {
                userService.Create(user);
                return CreatedAtRoute("GetUser", new { id = user.Id.ToString() }, user);
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() });
            }
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, User userIn)
        {
            try
            {
                var user = userService.Get(id);

                if (user == null)
                {
                    return NotFound();
                }

                userService.Update(id, userIn);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() });
            }
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            try
            {
                var user = userService.Get(id);

                if (user == null)
                {
                    return NotFound();
                }

                userService.Remove(user.Id);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() });
            }
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] AuthenticateRequest user)
        {
            try
            {
                var result = userService.Authenticate(user);

                if (result != null)
                {
                    return Ok(result);
                }
                else return BadRequest(new { message = "Username or password is incorrect" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() });
            }
        }

    }
}
