﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DnsClient.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MLTrainer.Common;
using MLTrainer.DTO;
using MLTrainer.Interfaces;
using MLTrainer.Models;
using MLTrainer.Security;
using MLTrainer.Services;
using MongoDB.Driver;
using Newtonsoft.Json.Schema;
using static MLTrainer.Common.CommonData;

namespace MLTrainer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrainedModelController : ControllerBase
    {
        private readonly ITrainedModelService trainedModelsService;
        private readonly IUserService userService;
        private readonly IAlgorithmService algorithmService;
        private readonly ILogger<TrainedModelController> logger;
        private AppSettings appSettings;
        private IMapper mapper;

        public TrainedModelController(ITrainedModelService trainedModelsService, IUserService userService, IAlgorithmService algorithmService, IOptions<AppSettings> appSettings, IMapper mapper, ILogger<TrainedModelController> logger)
        {
            this.trainedModelsService = trainedModelsService;
            this.userService = userService;
            this.algorithmService = algorithmService;
            this.appSettings = appSettings.Value;
            this.mapper = mapper;
            this.logger = logger;
        }

        [Authorize]
        [HttpGet("getbytitle/{title}")]
        public ActionResult<TrainedModelResponse> GetTrainedModel(string title)
        {
            try
            {
                var trainedModel = trainedModelsService.GetByTitle(title);
                var trainedModelResponse = mapper.Map<TrainedModelResponse>(trainedModel);
                return trainedModelResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpGet("getbyuserid/{id:length(24)}")]
        public ActionResult<List<TrainedModelResponse>> GetTrainedModels(string id)
        {
            try
            {
                var trainedModels = trainedModelsService.GetByUserId(id);
                var trainedModelsResponse = mapper.Map<List<TrainedModel>, List<TrainedModelResponse>>(trainedModels);
                return trainedModelsResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpGet("getallavailable")]
        public ActionResult<List<TrainedModelResponse>> GetAllAvailableTrainedModels()
        {
            try
            {
                var user = (UserResponse)HttpContext.Items["User"];
                var trainedModels = trainedModelsService.GetByUserIdAndCommon(user?.Id);
                var trainedModelsResponse = mapper.Map<List<TrainedModel>, List<TrainedModelResponse>>(trainedModels);
                return trainedModelsResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpGet("{id:length(24)}", Name = "GetTrainedModel")]
        public ActionResult<TrainedModelResponse> Get(string id)
        {
            try
            {
                var trainedModel = trainedModelsService.Get(id);

                if (trainedModel == null)
                {
                    return NotFound();
                }
                var trainedModelResponse = mapper.Map<TrainedModelResponse>(trainedModel);
                return trainedModelResponse;
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpPost("create")]
        public ActionResult<TrainedModelResponse> Create([FromBody] TrainedModelRequest trainedModelRequest)
        {
            try
            {
                var trainedModel = mapper.Map<TrainedModel>(trainedModelRequest);
                var user = (UserResponse)HttpContext.Items["User"];
                trainedModel.UserId = user?.Id;
                trainedModel.Status = Status.Ready.ToString();

                trainedModel.Path = user?.Id + @"\trainedModels\" + trainedModel.Title.Replace(" ", "_") + @"\script.py";
                string path = Path.Combine(appSettings.DataLocation, trainedModel.Path);

                if (appSettings.IsLinux == 1)
                {
                    trainedModel.Path = trainedModel.Path.Replace(@"\", "/");
                    path = path.Replace(@"\", "/");
                }
                Directory.CreateDirectory(Path.GetDirectoryName(path));

                var algorithm = algorithmService.Get(trainedModel.Algorithm.Id);
                string algPath = Path.Combine(appSettings.DataLocation, algorithm.Path);
                string algorithmScript = System.IO.File.ReadAllText(algPath);

                for (int hpIndex = 0; hpIndex < trainedModel.Hyperparams.Count; hpIndex++)
                {
                    algorithmScript = algorithmScript.Replace("#hp" + hpIndex + "#", trainedModel.Hyperparams[hpIndex]);
                }
                algorithmScript = algorithmScript + @"
json_string = model.to_json()
open('/scripts/model.json', 'w').write(json_string)";

                System.IO.File.WriteAllText(path, algorithmScript);

                trainedModelsService.Create(trainedModel);
                return CreatedAtRoute("GetTrainedModel", new { id = trainedModel.Id.ToString() }, trainedModel);
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = ex.Message.ToString() });
            }
        }

        [Authorize]
        [HttpGet("download/{id:length(24)}")]
        public IActionResult DownloadDocument(string id)
        {
            var trainedModel = trainedModelsService.Get(id);

            if (trainedModel == null)
            {
                return NotFound();
            }
            var user = (UserResponse)HttpContext.Items["User"];
            if (trainedModel.UserId != user.Id)
            {
                return BadRequest(new
                {
                    Message = "You do not have rights to download this TrainedModel."
                });
            }

            string fileName = trainedModel.Title;

            byte[] fileBytes = System.IO.File.ReadAllBytes(Path.Combine(Path.GetDirectoryName(Path.Combine(appSettings.DataLocation, trainedModel.Path)), "model.json"));

            return File(fileBytes, "application/force-download", fileName);

        }

        [Authorize]
        [HttpGet("startTraining/{id:length(24)}")]
        public IActionResult StartTraining(string id)
        {
            try
            {

                var trainedModel = trainedModelsService.Get(id);

                if (trainedModel == null)
                {
                    return NotFound();
                }
                var user = (UserResponse)HttpContext.Items["User"];
                if (trainedModel.UserId != user.Id)
                {
                    return BadRequest(new
                    {
                        Message = "You do not have rights to train this Model."
                    });
                }
                var commonuser = userService.GetByUsername("Common");

                if (commonuser ==null)
                {
                    return BadRequest(new
                    {
                        Message = "Missing common user."
                    });
                }

                string arguments = "";
                if (appSettings.DifferentArgs == 0)
                {
                    //string arguments = "/c start notepad.exe";
                    arguments = appSettings.StartingArgs + "run -v " + appSettings.DataLocation + commonuser.Id + "/datasets:/commondatasets -v "
                        + appSettings.DataLocation + user.Id + "/datasets:/datasets -v " + Path.GetDirectoryName(Path.Combine(appSettings.DataLocation, trainedModel.Path))
                        + ":/scripts -it --rm " + appSettings.DockerImageTitle + appSettings.EndingArgs;
                }
                else
                {
                    arguments = appSettings.StartingArgs;
                }
                logger.LogInformation(arguments);
                string processName = trainedModel.Title.Replace(" ", "_");


                //ProcessStartInfo startInfo = new ProcessStartInfo() { FileName = "/bin/bash", Arguments = arguments, };
                //ProcessStartInfo startInfo = new ProcessStartInfo() { FileName = appSettings.AppName, Arguments = arguments, Verb = processName,}; 
                ProcessStartInfo startInfo = new ProcessStartInfo() { FileName = appSettings.AppName, Arguments = arguments, UseShellExecute = true , };
                Process proc = new Process() { StartInfo = startInfo, };
                proc.Start();

                CommonData.trackedProcessesDict.TryAdd(trainedModel.Id.ToString(), new TrackedProcess { PID = proc.Id, ProcessName = processName });
                logger.LogInformation(trainedModel.Id + " | " + trainedModel.Title + " | " + proc.Id);

                trainedModelsService.UpdateTrainedModelStatus(trainedModel.Id, Status.Training);

                return Ok(new
                {
                    Message = "Training started!"
                });
            }
            catch (Exception ex)
            {
                logger.LogInformation(ex.ToString());
                return BadRequest(new
                {
                    Message = "Server error, cannot train model."
                });
            }

        }

        [Authorize]
        [HttpGet("getConsoleLog/{id:length(24)}")]
        public IActionResult GetConsoleLog(string id)
        {
            var trainedModel = trainedModelsService.Get(id);

            if (trainedModel == null)
            {
                return NotFound();
            }

            var user = (UserResponse)HttpContext.Items["User"];
            if (trainedModel.UserId != user.Id)
            {
                return BadRequest(new
                {
                    Message = "You do not have rights to get this Model!"
                });
            }

            if (trainedModel.Status != Status.Finished.ToString())
            {
                return BadRequest(new
                {
                    Message = "TrainedModel must be trained before downloading!"
                });
            }
            string consoleLog;
            try
            {
                logger.LogInformation(Path.Combine(appSettings.DataLocation, Path.GetDirectoryName(trainedModel.Path), "console.log"));
                consoleLog = System.IO.File.ReadAllText(Path.Combine(appSettings.DataLocation, Path.GetDirectoryName(trainedModel.Path), "console.log"));
            }
            catch(Exception ex)
            {
                logger.LogInformation(ex.ToString());
                return NotFound(new
                {
                    Message = "There are no console logs available for this trained model!"
                });
            }

            return Ok( new { ConsoleLog = consoleLog });

        }

        [Authorize]
        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, TrainedModelRequest trainedModelRequestIn)
        {
            try
            {
                var trainedModel = mapper.Map<TrainedModel>(trainedModelRequestIn);
                var trainedModelDB = trainedModelsService.Get(id);

                if (trainedModelDB == null)
                {
                    return NotFound();
                }

                trainedModel.UserId = trainedModelDB.UserId;
                trainedModel.Status = trainedModelDB.Status;

                trainedModel.Path = trainedModelDB.UserId + @"\trainedModels\" + trainedModel.Title.Replace(" ", "_") + @"\script.py";
                string path = Path.Combine(appSettings.DataLocation, trainedModel.Path);

                if (appSettings.IsLinux == 1)
                {
                    trainedModel.Path = trainedModel.Path.Replace(@"\", "/");
                    path = path.Replace(@"\", "/");
                }

                Directory.CreateDirectory(Path.GetDirectoryName(path));

                var algorithm = algorithmService.Get(trainedModel.Algorithm.Id);
                string algPath = Path.Combine(appSettings.DataLocation, algorithm.Path);
                string algorithmScript = System.IO.File.ReadAllText(algPath);

                for (int hpIndex = 0; hpIndex < trainedModel.Hyperparams.Count; hpIndex++)
                {
                    algorithmScript.Replace("#hp" + hpIndex + "#", trainedModel.Hyperparams[hpIndex]);
                }

                System.IO.File.WriteAllText(path, algorithmScript);


                trainedModelsService.Update(id, trainedModel);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }

        [Authorize]
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            try
            {
                var trainedModel = trainedModelsService.Get(id);
                
                if (trainedModel == null)
                {
                    return NotFound();
                }
                var user = (UserResponse)HttpContext.Items["User"];

                if (trainedModel.UserId != user.Id)
                {
                    return BadRequest(new {
                        Message = "You do not have rights to delete this TrainedModel."
                    });
                }

                System.IO.Directory.Delete(Path.GetDirectoryName(Path.Combine(appSettings.DataLocation, trainedModel.Path)), true);
                trainedModelsService.Remove(trainedModel.Id);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    Message = ex.Message.ToString()
                });
            }
        }
    }
}
