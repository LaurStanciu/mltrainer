﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Models
{
    public class MLTrainerDatabaseSettings : IMLTrainerDatabaseSettings
    {
        public string UsersCollectionName { get; set; }
        public string TrainedModelsCollectionName { get; set; }
        public string AlgorithmCollectionName { get; set; }
        public string DatasetCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IMLTrainerDatabaseSettings
    {
        string UsersCollectionName { get; set; }
        string TrainedModelsCollectionName { get; set; }
        string AlgorithmCollectionName { get; set; }
        string DatasetCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
