﻿using AutoMapper;
using MLTrainer.DTO;
using MLTrainer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Profiles
{
    public class AlgorithmProfile : Profile
    {
        public AlgorithmProfile()
        {
            CreateMap<AlgorithmRequest, Algorithm>();
            CreateMap<Algorithm, AlgorithmResponse>();
        }
    }
}
