﻿using AutoMapper;
using MLTrainer.DTO;
using MLTrainer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Profiles
{
    public class UserProfile :Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserResponse>().ReverseMap();
        }
    }
}
