﻿using AutoMapper;
using MLTrainer.DTO;
using MLTrainer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MLTrainer.Profiles
{
    public class TrainedModelProfile : Profile
    {
        public TrainedModelProfile()
        {
            CreateMap<TrainedModelRequest, TrainedModel>();
            CreateMap<TrainedModel, TrainedModelResponse>();
        }
    }
}
