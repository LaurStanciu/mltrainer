import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    margin: theme.spacing(1),
  },
}));

export default function Dataset(props) {
  const classes = useStyles();

  return (
    <Paper className={classes.paper}>
      <Grid container spacing={1} direction="row">
        <Grid
          container
          item
          xs={11}
          justify="center"
          alignItems="stretch"
          direction="column"
        >
          <Grid item xs={12}>
            <Typography variant="h6">Title: {props.dataset.title}</Typography>
            <Typography variant="h6">
              Description: {props.dataset.description}
            </Typography>
            <Typography variant="h6">Size: {props.dataset.size}</Typography>
          </Grid>
        </Grid>
        <Grid
          container
          item
          xs={1}
          justify="center"
          alignItems="stretch"
          direction="column"
        >
          <Grid item>
            <IconButton
              onClick={() => props.handleDeleteDataset(props.dataset.id)}
            >
              <DeleteForeverIcon />
            </IconButton>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  );
}
