import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import NotesIcon from "@material-ui/icons/Notes";
import GetAppIcon from "@material-ui/icons/GetApp";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    margin: theme.spacing(1),
  },
}));

export default function TrainedModel(props) {
  const classes = useStyles();

  return (
    <Paper className={classes.paper}>
      <Grid container direction="row">
        <Grid container item xs={11} alignItems="stretch" direction="column">
          <Grid item xs={12}>
            <Typography variant="h6">Title: {props.model.title}</Typography>
            <Typography variant="h6">
              Description: {props.model.description}
            </Typography>
            <Typography variant="h6">
              Algorithm: {props.model.algorithm.title}
            </Typography>
            {props.model.hyperparams.length !== 0 ? (
              <Typography variant="h6">
                Hyperparams: {props.model.hyperparams.join(", ")}
              </Typography>
            ) : null}
            <Typography variant="h6">Status: {props.model.status}</Typography>
          </Grid>
        </Grid>
        <Grid
          container
          item
          xs={1}
          justify="center"
          alignItems="stretch"
          direction="column"
        >
          <Grid item>
            <IconButton
              onClick={() => props.handleStartTraining(props.model.id)}
            >
              <PlayCircleOutlineIcon />
            </IconButton>
          </Grid>
          {props.model.status === "Finished" ? (
            <Grid item>
              <IconButton
                onClick={() => props.handleGetConsoleLog(props.model.id)}
              >
                <NotesIcon />
              </IconButton>
            </Grid>
          ) : null}
          {props.model.status === "Finished" ? (
            <Grid item>
              <IconButton
                onClick={() => props.handleDownloadTrainedModel(props.model.id)}
              >
                <GetAppIcon />
              </IconButton>
            </Grid>
          ) : null}
          <Grid item>
            <IconButton
              onClick={() => props.handleDeleteTrainedModel(props.model.id)}
            >
              <DeleteForeverIcon />
            </IconButton>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  );
}
