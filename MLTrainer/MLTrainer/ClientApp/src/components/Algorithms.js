import React, { useState, useEffect, useRef } from "react";
import Title from "./Title";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Algorithm from "./common/Algorithm";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import InputLabel from "@material-ui/core/InputLabel";
import DialogTitle from "@material-ui/core/DialogTitle";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { algorithmService } from "../services/algorithm.service";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    paddingTop: 20,
    paddingBottom: 8,
  },
  centerAlignDialogActions: {
    justifyContent: "center",
  },
  input: {
    marginTop: 1,
    marginBottom: 1,
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Algorithms(props) {
  const textAreaRef = useRef();
  const classes = useStyles();
  const [algorithms, setAlgorithms] = useState([]);
  const [messageObj, setMessageObj] = React.useState({
    message: "",
    severity: "",
  });
  const [isFormValid, setIsFormValid] = useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [open, setOpen] = React.useState(false);

  const [algorithmRequestTitle, setAlgorithmRequestTitle] = useState("");

  const [
    algorithmRequestDescription,
    setAlgorithmRequestDescription,
  ] = useState("");

  const [
    algorithmRequestFileContent,
    setAlgorithmRequestFileContent,
  ] = useState("");

  useEffect(() => {
    if (algorithmRequestTitle.length > 0) {
      setIsFormValid(true);
    } else {
      setIsFormValid(false);
    }
  }, [
    algorithmRequestTitle,
    setAlgorithmRequestTitle,
    algorithmRequestDescription,
    setAlgorithmRequestDescription,
    algorithmRequestFileContent,
    setAlgorithmRequestFileContent,
  ]);

  useEffect(() => {
    algorithmService.getAlgorithms().then(
      (algorithmsResponse) => {
        var algo = [...algorithms, ...algorithmsResponse];
        setAlgorithms(algo);
      },
      (error) => {
        console.log(error);
        setMessageObj({
          message: error,
          severity: "error",
        });

        handleClickSnackbar();
      }
    );
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickSnackbar = () => {
    setOpenSnackbar(true);
  };

  const handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenSnackbar(false);
  };

  const handleChangeTitle = (event) => {
    setAlgorithmRequestTitle(event.target.value);
  };

  const handleChangeDescription = (event) => {
    setAlgorithmRequestDescription(event.target.value);
  };

  const handleChangeFileContent = (event) => {
    setAlgorithmRequestFileContent(event.target.value);
  };

  const resetCreateDialog = () => {
    setAlgorithmRequestTitle("");
    setAlgorithmRequestDescription("");
    setAlgorithmRequestFileContent("");
  };

  const addAlgorithm = (e) => {
    e.preventDefault();
    algorithmService
      .addAlgorithm({
        title: algorithmRequestTitle,
        description: algorithmRequestDescription,
        filecontent: algorithmRequestFileContent,
      })
      .then(
        (algorithm) => {
          setMessageObj({
            message: "Success!",
            severity: "success",
          });

          algorithms.push({
            id: algorithm.id,
            title: algorithm.title,
            description: algorithm.description,
          });
          handleClickSnackbar();
          resetCreateDialog();
          handleClose();
        },
        (error) => {
          console.log(error);
          setMessageObj({
            message: error,
            severity: "error",
          });

          handleClickSnackbar();
        }
      );
  };

  const handleKeyDown = (e) => {
    if (e.key === "Tab") {
      e.preventDefault();

      const { selectionStart, selectionEnd } = e.target;

      const newValue =
        algorithmRequestFileContent.substring(0, selectionStart) +
        "\t" +
        algorithmRequestFileContent.substring(selectionEnd);

      setAlgorithmRequestFileContent(newValue);
      if (textAreaRef.current) {
        textAreaRef.current.value = newValue;
        textAreaRef.current.selectionStart = textAreaRef.current.selectionEnd =
          selectionStart + 2;
      }
    }
  };

  const deleteAlgorithm = (e) => {
    algorithmService.deleteAlgorithm(e).then(
      () => {
        setMessageObj({
          message: "Success!",
          severity: "success",
        });

        algorithms.splice(
          algorithms.findIndex((x) => x.id === e),
          1
        );
        handleClickSnackbar();
      },
      (error) => {
        console.log(error);
        setMessageObj({
          message: error,
          severity: "error",
        });

        handleClickSnackbar();
      }
    );
  };

  return (
    <>
      <Snackbar
        open={openSnackbar}
        autoHideDuration={3000}
        onClose={(e, r) => handleCloseSnackbar(e, r)}
      >
        <Alert
          onClose={(e, r) => handleCloseSnackbar(e, r)}
          severity={messageObj.severity}
        >
          {messageObj.message}
        </Alert>
      </Snackbar>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        fullWidth
        fullScreen
      >
        <DialogTitle id="form-dialog-title">Add algorithm</DialogTitle>
        <DialogContent>
          <InputLabel id="title-label">Title</InputLabel>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="title"
            label="Title"
            name="title"
            value={algorithmRequestTitle}
            autoComplete="title"
            autoFocus
            onChange={handleChangeTitle}
          />
          <InputLabel id="description-label">Description</InputLabel>
          <TextField
            variant="outlined"
            margin="normal"
            rows={2}
            required
            fullWidth
            id="description"
            label="Description"
            name="description"
            value={algorithmRequestDescription}
            autoComplete="description"
            onChange={handleChangeDescription}
          />

          <InputLabel id="hyperparams-label">Code</InputLabel>
          <TextField
            // className={classes.textFld}
            multiline
            fullWidth
            rows={28}
            variant="outlined"
            margin="normal"
            value={algorithmRequestFileContent}
            ref={textAreaRef}
            onKeyDown={handleKeyDown}
            onChange={handleChangeFileContent}
          ></TextField>
        </DialogContent>
        <DialogActions className={classes.centerAlignDialogActions}>
          <Button onClick={handleClose} variant="contained" color="primary">
            Cancel
          </Button>
          <Button
            onClick={addAlgorithm}
            disabled={!isFormValid}
            variant="contained"
            color="primary"
          >
            Add
          </Button>
        </DialogActions>
      </Dialog>
      <Grid container justify="center">
        <Grid item lg={6} md={8} sm={10} xs={12}>
          <Grid
            direction="column"
            alignItems="stretch"
            container
            justify="center"
          >
            <Grid>
              <Title>Algorithms</Title>
            </Grid>

            <Box className={classes.fab} mb={2}>
              <Fab color="primary" aria-label="add" onClick={handleClickOpen}>
                <AddIcon />
              </Fab>
            </Box>
            {algorithms.length > 0 ? (
              <Grid>
                {algorithms.map((alg) => (
                  <Algorithm
                    algorithm={alg}
                    key={alg.id}
                    handleDeleteAlgorithm={deleteAlgorithm}
                  ></Algorithm>
                ))}
              </Grid>
            ) : (
              <Grid>
                <Typography align="center">
                  There are no available Algorithms
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
