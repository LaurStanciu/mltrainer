import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Copyright from "./common/Copyright";

const useStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: theme.palette.background.paper,
    // marginTop: theme.spacing(8),
    // position: "fixed",
    // bottom: 0,
    // height: 5,
    position: "absolute",
    left: 0,
    bottom: 0,
    right: 0,
  },
}));

export default function Footer() {
  const classes = useStyles();

  return (
    <footer className={classes.footer}>
      <Copyright />
    </footer>
  );
}
