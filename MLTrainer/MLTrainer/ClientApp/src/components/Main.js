import React from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Title from "./Title";
import AccountCircle from "@material-ui/icons/AccountCircle";
import DashboardIcon from "@material-ui/icons/Dashboard";
import CodeIcon from "@material-ui/icons/Code";
import FolderIcon from "@material-ui/icons/Folder";

// const useStyles = makeStyles((theme) => ({
//   footer: {
//     backgroundColor: theme.palette.background.paper,
//     // marginTop: theme.spacing(8),
//     padding: theme.spacing(6, 0),
//     marginTop: 5 % +60,
//     bottom: 0,
//   },
// }));

export default function Main() {
  // const classes = useStyles();

  return (
    <>
      <Grid container justify="center">
        <Grid item lg={8} md={8} sm={10} xs={12}>
          <Grid
            direction="column"
            alignItems="stretch"
            container
            justify="center"
          >
            <Grid mb={2}>
              <Title>Tutorial</Title>
            </Grid>
            <Grid>
              <Typography component="p" variant="body1">
                Click <AccountCircle /> to access your account's options.
              </Typography>
              <Typography component="p" variant="body1">
                Click <DashboardIcon /> to access your Trained Models. Please
                note the you cannot create a TrainedModel if you do not have any
                algorithms.
              </Typography>
              <Typography component="p" variant="body1">
                Click <CodeIcon /> to access your Algorithms.
              </Typography>
              <Typography component="p" variant="body1">
                Click <FolderIcon /> to access your Datasets.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
