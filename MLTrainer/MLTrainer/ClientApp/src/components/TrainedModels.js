import React, { useState, useEffect } from "react";
import { HubConnectionBuilder } from "@microsoft/signalr";
import Title from "./Title";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import TrainedModel from "./common/TrainedModel";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import DialogTitle from "@material-ui/core/DialogTitle";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { trainedModelService } from "../services/trainedModels.service";
import { algorithmService } from "../services/algorithm.service";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    paddingTop: 20,
    paddingBottom: 8,
  },
  centerAlignDialogActions: {
    justifyContent: "center",
  },
  input: {
    marginTop: 1,
    marginBottom: 1,
  },
}));

// const algorithms = [
//   {
//     id: "aaa",
//     title: "alg1",
//   },
//   {
//     id: "bbb",
//     title: "alg2",
//   },
// ];

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function TrainedModels(props) {
  const classes = useStyles();
  const [messageObj, setMessageObj] = React.useState({
    message: "",
    severity: "",
  });

  const [hubConnect, setHubConnect] = useState(null);
  const [modelToUpdate, setModelToUpdate] = useState("");

  const [models, setModels] = useState([]);
  const [algorithms, setAlgorithms] = useState([]);

  const [isFormValid, setIsFormValid] = useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [open, setOpen] = React.useState(false);

  const [trainedModelRequestTitle, setTrainedModelRequestTitle] = useState("");
  const [
    trainedModelRequestDescription,
    setTrainedModelRequestDescription,
  ] = useState("");

  const [
    trainedModelRequestHyperParams,
    setTrainedModelRequestHyperParams,
  ] = useState("");

  const [trainedModelConsoleLog, setTrainedModelConsoleLog] = useState("");

  const [openConsoleLog, setOpenConsoleLog] = useState(false);

  const [algorithm, setAlgorithm] = React.useState({
    id: "",
    title: "",
  });

  useEffect(() => {
    if (
      trainedModelRequestTitle.length > 0 &&
      trainedModelRequestDescription.length > 0 &&
      algorithm.title.length > 0
    ) {
      setIsFormValid(true);
    } else {
      setIsFormValid(false);
    }
  }, [trainedModelRequestTitle, trainedModelRequestDescription, algorithm]);

  useEffect(() => {
    trainedModelService.getTrainedModels().then(
      (trainedModelsResponse) => {
        var data = [...models, ...trainedModelsResponse];
        setModels(data);
      },
      (error) => {
        console.log(error);
        setMessageObj({
          message: error,
          severity: "error",
        });

        handleClickSnackbar();
      }
    );

    algorithmService.getAlgorithms().then(
      (algorithmsResponse) => {
        var algo = [...algorithms, ...algorithmsResponse];
        setAlgorithms(algo);
        console.log(algo);
      },
      (error) => {
        console.log(error);
        setMessageObj({
          message: error,
          severity: "error",
        });

        handleClickSnackbar();
      }
    );
  }, []);

  useEffect(() => {
    var url = process.env.REACT_APP_BASE_URL + "/chathub";
    const newConnection = new HubConnectionBuilder()
      .withUrl(url)
      .withAutomaticReconnect()
      .build();

    setHubConnect(newConnection);
  }, []);

  useEffect(() => {
    if (hubConnect) {
      hubConnect
        .start()
        .then((result) => {
          console.log("Connected!");

          hubConnect.on("UpdateStatus", (id) => {
            setModelToUpdate(id);
          });
        })
        .catch((e) => console.log("Connection failed: ", e));
    }
  }, [hubConnect]);

  useEffect(() => {
    if (modelToUpdate !== "") {
      updateModel(modelToUpdate);
      setModelToUpdate("");
    }
  }, [modelToUpdate]);

  const updateModel = (id) => {
    let newList = [...models];
    var index = models.findIndex((x) => x.id === id);
    newList[index].status = "Finished";

    setModels(newList);
  };
  const handleClickOpen = () => {
    if (algorithms.length > 0) {
      setOpen(true);
    } else {
      setMessageObj({
        message:
          "No algorithms available, add algorithms in order to add TrainedModels.",
        severity: "error",
      });
      handleClickSnackbar();
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleCloseConsoleLog = () => {
    setOpenConsoleLog(false);
  };

  const handleClickSnackbar = () => {
    setOpenSnackbar(true);
  };

  const handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenSnackbar(false);
  };

  const handleChangeTitle = (event) => {
    setTrainedModelRequestTitle(event.target.value);
  };

  const handleChangeDescription = (event) => {
    setTrainedModelRequestDescription(event.target.value);
  };

  const handleChangeHyperParams = (event) => {
    setTrainedModelRequestHyperParams(event.target.value);
  };

  const handleChangeAlgorithm = (event) => {
    if (event.target.value === "") {
      setAlgorithm({ id: "", title: "" });
    } else {
      setAlgorithm({
        id: algorithms.find((obj) => {
          return obj.title === event.target.value;
        }).id,
        title: event.target.value,
      });
    }
  };

  const resetCreateDialog = () => {
    setTrainedModelRequestTitle("");
    setTrainedModelRequestDescription("");
    setAlgorithm({ id: "", title: "" });
    setTrainedModelRequestHyperParams("");
  };

  const addTrainedModel = (e) => {
    e.preventDefault();
    var hyperparamsRequest = trainedModelRequestHyperParams.trim().split(";");
    var filteredHyperParams = hyperparamsRequest.filter(function (el) {
      return el !== "";
    });
    console.log(filteredHyperParams);
    trainedModelService
      .addTrainedModel({
        title: trainedModelRequestTitle,
        description: trainedModelRequestDescription,
        algorithm: { ...algorithm },
        hyperparams: filteredHyperParams,
      })
      .then(
        (trainedModel) => {
          setMessageObj({
            message: "Success!",
            severity: "success",
          });

          models.push({
            id: trainedModel.id,
            title: trainedModel.title,
            description: trainedModel.description,
            status: trainedModel.status,
            hyperparams: trainedModel.hyperparams,
            algorithm: { ...trainedModel.algorithm },
          });
          handleClickSnackbar();
          resetCreateDialog();
          handleClose();
        },
        (error) => {
          console.log(error);
          setMessageObj({
            message: error,
            severity: "error",
          });

          handleClickSnackbar();
        }
      );
  };

  const deleteTrainedModel = (e) => {
    trainedModelService.deleteTrainedModel(e).then(
      () => {
        setMessageObj({
          message: "Success!",
          severity: "success",
        });

        models.splice(
          models.findIndex((x) => x.id === e),
          1
        );
        handleClickSnackbar();
      },
      (error) => {
        console.log(error);
        setMessageObj({
          message: error,
          severity: "error",
        });

        handleClickSnackbar();
      }
    );
  };

  const startTraining = (e) => {
    trainedModelService.startTraining(e).then(
      () => {
        let newList = [...models];
        var index = models.findIndex((x) => x.id === e);
        newList[index].status = "Training";

        setModels(newList);

        setMessageObj({
          message: "Started to train model: " + newList[index].title + "!",
          severity: "success",
        });

        handleClickSnackbar();
      },
      (error) => {
        console.log(error);
        setMessageObj({
          message: error,
          severity: "error",
        });

        handleClickSnackbar();
      }
    );
  };

  const downloadTrainedModel = (e) => {
    trainedModelService.downloadTrainedModel(e).then(
      () => {},
      (error) => {
        console.log(error);
        setMessageObj({
          message: error,
          severity: "error",
        });

        handleClickSnackbar();
      }
    );
  };

  const getConsoleOutput = (e) => {
    trainedModelService.getConsoleOutput(e).then(
      (response) => {
        setTrainedModelConsoleLog(response.consoleLog);

        setOpenConsoleLog(true);
      },
      (error) => {
        console.log(error);
        setMessageObj({
          message: error,
          severity: "error",
        });

        handleClickSnackbar();
      }
    );
  };

  return (
    <>
      <Snackbar
        open={openSnackbar}
        autoHideDuration={3000}
        onClose={(e, r) => handleCloseSnackbar(e, r)}
      >
        <Alert
          onClose={(e, r) => handleCloseSnackbar(e, r)}
          severity={messageObj.severity}
        >
          {messageObj.message}
        </Alert>
      </Snackbar>
      <Dialog
        open={openConsoleLog}
        onClose={handleCloseConsoleLog}
        aria-labelledby="form-dialog-title"
        fullWidth
        maxWidth="lg"
      >
        <DialogTitle id="form-dialog-title">Console log</DialogTitle>
        <DialogContent>
          <Typography style={{ whiteSpace: "pre-line" }}>
            {trainedModelConsoleLog}
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseConsoleLog} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        maxWidth="sm"
      >
        <DialogTitle id="form-dialog-title">Add model to train</DialogTitle>
        <DialogContent>
          <InputLabel id="title-label"></InputLabel>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="title"
            label="Title"
            name="title"
            value={trainedModelRequestTitle}
            autoComplete="title"
            autoFocus
            onChange={handleChangeTitle}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="description"
            label="Description"
            name="description"
            value={trainedModelRequestDescription}
            autoComplete="description"
            onChange={handleChangeDescription}
          />
          <Box mt={2}>
            <Typography id="algorithm-label"> Algorithm</Typography>

            <Select
              fullWidth
              labelId="algorithm-label"
              id="algorithm"
              value={algorithm.title}
              onChange={handleChangeAlgorithm}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {algorithms.map((currentAlgorithm) => (
                <MenuItem
                  value={currentAlgorithm.title}
                  key={currentAlgorithm.id}
                >
                  {currentAlgorithm.title}
                </MenuItem>
              ))}
            </Select>
          </Box>
          <Box mt={4}>
            <Typography id="hyperparams-label">
              Use semicolon (;) to separate hyperparams
            </Typography>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              id="hyperparams"
              label="Hyperparams"
              name="hyperparams"
              value={trainedModelRequestHyperParams}
              autoComplete="hyperparams"
              onChange={handleChangeHyperParams}
            />
          </Box>
        </DialogContent>
        <DialogActions className={classes.centerAlignDialogActions}>
          <Button onClick={handleClose} variant="contained" color="primary">
            Cancel
          </Button>
          <Button
            onClick={addTrainedModel}
            disabled={!isFormValid}
            variant="contained"
            color="primary"
          >
            Add
          </Button>
        </DialogActions>
      </Dialog>
      <Grid container justify="center">
        <Grid item lg={6} md={8} sm={10} xs={12}>
          <Grid
            direction="column"
            alignItems="stretch"
            container
            justify="center"
          >
            <Grid>
              <Title>Trained Models</Title>
            </Grid>

            <Box className={classes.fab} mb={2}>
              <Fab color="primary" aria-label="add" onClick={handleClickOpen}>
                <AddIcon />
              </Fab>
            </Box>
            {models.length > 0 ? (
              <Grid>
                {models.map((currentModel) => (
                  <TrainedModel
                    model={currentModel}
                    key={currentModel.id}
                    handleDeleteTrainedModel={deleteTrainedModel}
                    handleDownloadTrainedModel={downloadTrainedModel}
                    handleStartTraining={startTraining}
                    handleGetConsoleLog={getConsoleOutput}
                  ></TrainedModel>
                ))}
              </Grid>
            ) : (
              <Grid>
                <Typography align="center">
                  There are no available TrainedModels
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
