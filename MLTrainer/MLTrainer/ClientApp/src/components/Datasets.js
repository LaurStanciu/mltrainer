import React, { useState, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import Title from "./Title";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Dataset from "./common/Dataset";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import InputLabel from "@material-ui/core/InputLabel";
import DialogTitle from "@material-ui/core/DialogTitle";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { datasetService } from "../services/dataset.service";
import { DropzoneArea } from "material-ui-dropzone";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    paddingTop: 20,
    paddingBottom: 8,
  },
  centerAlignDialogActions: {
    justifyContent: "center",
  },
  input: {
    marginTop: 1,
    marginBottom: 1,
  },
}));

// const datasets = [
//   {
//     id: "aaa",
//     title: "dataset1",
//     description: "desc1",
//     size: 24,
//   },
//   {
//     id: "bbb",
//     title: "dataset2",
//     description: "desc2",
//     size: 24,
//   },
// ];

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Datasets(props) {
  const classes = useStyles();
  // const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  const [messageObj, setMessageObj] = React.useState({
    message: "",
    severity: "",
  });

  const [datasets, setDatasets] = useState([]);

  const [isFormValid, setIsFormValid] = useState(false);
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [open, setOpen] = React.useState(false);

  const [datasetRequestTitle, setDatasetRequestTitle] = useState("");

  const [datasetRequestDescription, setDatasetRequestDescription] = useState(
    ""
  );

  const [datasetRequestFileName, setDatasetRequestFileName] = useState("");
  const [datasetRequestFile, setDatasetRequestFile] = useState();
  const [datasetRequestFileSize, setDatasetRequestFileSize] = useState(0);

  useEffect(() => {
    if (
      datasetRequestTitle.length > 0 &&
      datasetRequestDescription.length > 0 &&
      datasetRequestFileName.length > 0 &&
      datasetRequestFileSize > 0
    ) {
      setIsFormValid(true);
    } else {
      setIsFormValid(false);
    }
  }, [
    datasetRequestTitle,
    setDatasetRequestTitle,
    datasetRequestDescription,
    setDatasetRequestDescription,
    datasetRequestFileName,
    setDatasetRequestFileName,
    datasetRequestFile,
    setDatasetRequestFile,
    datasetRequestFileSize,
    setDatasetRequestFileSize,
  ]);

  useEffect(() => {
    datasetService.getDatasets().then(
      (datasetsResponse) => {
        var data = [...datasets, ...datasetsResponse];
        setDatasets(data);
      },
      (error) => {
        console.log(error);
        setMessageObj({
          message: error,
          severity: "error",
        });

        handleClickSnackbar();
      }
    );
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickSnackbar = () => {
    setOpenSnackbar(true);
  };

  const handleCloseSnackbar = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenSnackbar(false);
  };

  const handleChangeTitle = (event) => {
    setDatasetRequestTitle(event.target.value);
  };

  const handleChangeDescription = (event) => {
    setDatasetRequestDescription(event.target.value);
  };

  const handleChangeFile = (event) => {
    if (event[0] !== undefined) {
      setDatasetRequestFile(event[0]);
      setDatasetRequestFileName(event[0].name);
      setDatasetRequestFileSize(event[0].size);
    }
  };

  const handleResetFile = () => {
    setDatasetRequestFileName("");
    setDatasetRequestFile();
    setDatasetRequestFileSize(0);
  };

  // const handleChangeFileName = (event) => {
  //   setDatasetRequestFileName(event.target.files[0].name);
  // };

  // const handleChangeFile = (event) => {
  //   setDatasetRequestFile(event.target.files[0]);
  // };

  // const handleChangeFileSize = (event) => {
  //   setDatasetRequestFileSize(event.target.value);
  // };

  const resetCreateDialog = () => {
    setDatasetRequestTitle("");
    setDatasetRequestDescription("");
    setDatasetRequestFileName("");
    setDatasetRequestFile();
    setDatasetRequestFileSize(0);
  };

  const addDataset = (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("title", datasetRequestTitle);
    formData.append("description", datasetRequestDescription);
    formData.append("filename", datasetRequestFileName);
    formData.append("formfile", datasetRequestFile);
    formData.append("size", datasetRequestFileSize);

    datasetService.addDataset(formData).then(
      (dataset) => {
        setMessageObj({
          message: "Success!",
          severity: "success",
        });

        datasets.push({
          id: dataset.id,
          title: dataset.title,
          description: dataset.description,
          size: dataset.size,
        });
        handleClickSnackbar();
        resetCreateDialog();
        handleClose();
      },
      (error) => {
        console.log(error);
        setMessageObj({
          message: error,
          severity: "error",
        });

        handleClickSnackbar();
      }
    );
  };

  const deleteDataset = (e) => {
    datasetService.deleteDataset(e).then(
      () => {
        setMessageObj({
          message: "Success!",
          severity: "success",
        });

        datasets.splice(
          datasets.findIndex((x) => x.id === e),
          1
        );
        handleClickSnackbar();
      },
      (error) => {
        console.log(error);
        setMessageObj({
          message: error,
          severity: "error",
        });

        handleClickSnackbar();
      }
    );
  };

  return (
    <>
      <Snackbar
        open={openSnackbar}
        autoHideDuration={3000}
        onClose={(e, r) => handleCloseSnackbar(e, r)}
      >
        <Alert
          onClose={(e, r) => handleCloseSnackbar(e, r)}
          severity={messageObj.severity}
        >
          {messageObj.message}
        </Alert>
      </Snackbar>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        maxWidth="sm"
      >
        <DialogTitle id="form-dialog-title">Add dataset</DialogTitle>
        <DialogContent>
          <InputLabel id="title-label"></InputLabel>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="title"
            label="Title"
            name="title"
            value={datasetRequestTitle}
            autoComplete="title"
            autoFocus
            onChange={handleChangeTitle}
          />
          <InputLabel id="description-label"></InputLabel>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="description"
            label="Description"
            name="description"
            value={datasetRequestDescription}
            autoComplete="description"
            onChange={handleChangeDescription}
          />
          <Typography></Typography>
          <InputLabel id="algorithm-label"></InputLabel>
          <DropzoneArea
            acceptedFiles={[".zip"]}
            showFileNames
            filesLimit={1}
            maxFileSize={3000000000} //3GB
            dropzoneText={
              "Drag and drop zip file here or click to browse your documents"
            }
            initialFiles={
              datasetRequestFile !== undefined ? [datasetRequestFile] : []
            }
            onChange={(file) => handleChangeFile(file)}
            onAlert={(message, variant) =>
              console.log(`${variant}: ${message}`)
            }
            onDelete={handleResetFile}
          />
        </DialogContent>
        <DialogActions className={classes.centerAlignDialogActions}>
          <Button onClick={handleClose} variant="contained" color="primary">
            Cancel
          </Button>
          <Button
            onClick={addDataset}
            disabled={!isFormValid}
            variant="contained"
            color="primary"
          >
            Add
          </Button>
        </DialogActions>
      </Dialog>
      <Grid container justify="center">
        <Grid item lg={6} md={8} sm={10} xs={12}>
          <Grid
            direction="column"
            alignItems="stretch"
            container
            justify="center"
          >
            <Grid>
              <Title>Datasets</Title>
            </Grid>

            <Box className={classes.fab} mb={2}>
              <Fab color="primary" aria-label="add" onClick={handleClickOpen}>
                <AddIcon />
              </Fab>
            </Box>
            {datasets.length > 0 ? (
              <Grid>
                {datasets.map((dataset) => (
                  <Dataset
                    dataset={dataset}
                    key={dataset.id}
                    handleDeleteDataset={deleteDataset}
                  ></Dataset>
                ))}
              </Grid>
            ) : (
              <Grid>
                <Typography align="center">
                  There are no available Datasets
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
