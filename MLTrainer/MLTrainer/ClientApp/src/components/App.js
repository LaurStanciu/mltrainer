import React from "react";

import SignIn from "./SignIn";
import SignUp from "./SignUp";
import Dashboard from "./Dashboard";
import { Route, Switch, Redirect } from "react-router-dom";
//import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { history } from "../helpers/history";
import { authenticationService } from "../services/authentication.service";
import TrainedModels from "./TrainedModels";
import Main from "./Main";
import Algorithms from "./Algorithms";
import Datasets from "./Datasets";

class App extends React.Component {
  state = {
    currentUser: null,
  };

  componentDidMount() {
    authenticationService.currentUser.subscribe((x) =>
      this.setState({ currentUser: x })
    );
  }

  logout() {
    authenticationService.logout();
    history.push("/signin");
  }

  render() {
    return (
      <>
        <ToastContainer autoClose={3000} hideProgressBar />

        <Switch>
          <Route
            path="/"
            exact
            render={() => {
              return <Dashboard history={history} children={<Main />} />;
            }}
          />
          <Route
            path="/trainedmodels"
            exact
            render={() => {
              return (
                <Dashboard history={history} children={<TrainedModels />} />
              );
            }}
          />
          <Route
            path="/algorithms"
            exact
            render={() => {
              return <Dashboard history={history} children={<Algorithms />} />;
            }}
          />
          <Route
            path="/datasets"
            exact
            render={() => {
              return <Dashboard history={history} children={<Datasets />} />;
            }}
          />
          <Route path="/signin" exact component={SignIn} />
          <Route path="/signup" exact component={SignUp} />
          <Redirect from="/*" to="/" />
        </Switch>
      </>
    );
  }
}

export default App;
//export default withRouter(App);
