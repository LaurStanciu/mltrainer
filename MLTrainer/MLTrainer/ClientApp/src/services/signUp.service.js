import { handleResponse } from "../helpers/handle-response";

export const signUpService = {
  signUp,
};

function signUp(username, email, role, password) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ username, email, role, password }),
  };

  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/user/create`,
    requestOptions
  ).then(handleResponse);
}
