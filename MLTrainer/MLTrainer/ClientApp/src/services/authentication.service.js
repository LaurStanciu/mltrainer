import { BehaviorSubject } from "rxjs";
import { handleResponse } from "../helpers/handle-response";

const currentUserSubject = new BehaviorSubject(
  JSON.parse(localStorage.getItem("currentUser"))
);

export const authenticationService = {
  isAuthenticated,
  login,
  logout,
  currentUser: currentUserSubject.asObservable(),
  get currentUserValue() {
    return currentUserSubject.value;
  },
};

function isAuthenticated() {
  var jwtDecode = require("jwt-decode");
  if (currentUserSubject.value && currentUserSubject.value.token) {
    var decodedToken = jwtDecode(currentUserSubject.value.token);
    var dateNow = new Date();
    if (dateNow.getTime() < decodedToken.exp * 1000) {
      return true;
    }
  }
  return false;
}

function login(username, password) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ username, password }),
  };

  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/user/login`,
    requestOptions
  )
    .then(handleResponse)
    .then((user) => {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem("currentUser", JSON.stringify(user));
      currentUserSubject.next(user);

      return user;
    });
}

function logout(history) {
  localStorage.removeItem("currentUser");
  currentUserSubject.next(null);
  history.push("/signin");
}
