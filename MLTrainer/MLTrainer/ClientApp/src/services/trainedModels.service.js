import { handleResponse } from "../helpers/handle-response";
import { handleResponseDownloadFile } from "../helpers/handle-response-download-file";
import { authHeader } from "../helpers/auth-header";

export const trainedModelService = {
  addTrainedModel,
  getTrainedModels,
  deleteTrainedModel,
  downloadTrainedModel,
  startTraining,
  getConsoleOutput,
};

function addTrainedModel(trainedModel) {
  const token = authHeader();
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: token.Authorization,
    },
    body: JSON.stringify(trainedModel),
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/trainedmodel/create`,
    requestOptions
  ).then(handleResponse);
}

function getTrainedModels() {
  const token = authHeader();
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: token.Authorization,
    },
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/trainedmodel/getallavailable`,
    requestOptions
  ).then(handleResponse);
}

function deleteTrainedModel(id) {
  const token = authHeader();
  const requestOptions = {
    method: "DELETE",
    headers: {
      Authorization: token.Authorization,
    },
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/trainedmodel/` + id,
    requestOptions
  ).then(handleResponse);
}

function downloadTrainedModel(id) {
  const token = authHeader();
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: token.Authorization,
    },
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/trainedmodel/download/` + id,
    requestOptions
  ).then(handleResponseDownloadFile);
}

function startTraining(id) {
  const token = authHeader();
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: token.Authorization,
    },
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/trainedmodel/startTraining/` + id,
    requestOptions
  ).then(handleResponse);
}

function getConsoleOutput(id) {
  const token = authHeader();
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: token.Authorization,
    },
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/trainedmodel/getConsoleLog/` + id,
    requestOptions
  ).then(handleResponse);
}
