import { handleResponse } from "../helpers/handle-response";
import { authHeader } from "../helpers/auth-header";

export const algorithmService = {
  addAlgorithm,
  getAlgorithms,
  deleteAlgorithm,
};

function addAlgorithm(algorithm) {
  const token = authHeader();
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: token.Authorization,
    },
    body: JSON.stringify(algorithm),
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/algorithm/create`,
    requestOptions
  ).then(handleResponse);
}

function getAlgorithms() {
  const token = authHeader();
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: token.Authorization,
    },
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/algorithm/getallavailable`,
    requestOptions
  ).then(handleResponse);
}

function deleteAlgorithm(id) {
  const token = authHeader();
  const requestOptions = {
    method: "DELETE",
    headers: {
      Authorization: token.Authorization,
    },
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/algorithm/` + id,
    requestOptions
  ).then(handleResponse);
}
