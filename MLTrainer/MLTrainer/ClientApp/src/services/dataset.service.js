import { handleResponse } from "../helpers/handle-response";
import { authHeader } from "../helpers/auth-header";

export const datasetService = {
  addDataset,
  getDatasets,
  deleteDataset,
};

function addDataset(dataset) {
  const token = authHeader();
  const requestOptions = {
    method: "POST",
    headers: {
      Authorization: token.Authorization,
    },
    body: dataset,
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/dataset/create`,
    requestOptions
  ).then(handleResponse);
}

function getDatasets() {
  const token = authHeader();
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: token.Authorization,
    },
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/dataset/getallavailable`,
    requestOptions
  ).then(handleResponse);
}

function deleteDataset(id) {
  const token = authHeader();
  const requestOptions = {
    method: "DELETE",
    headers: {
      Authorization: token.Authorization,
    },
  };
  return fetch(
    process.env.REACT_APP_BASE_URL + `/api/dataset/` + id,
    requestOptions
  ).then(handleResponse);
}
